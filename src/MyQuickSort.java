import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;
import org.apache.commons.lang.time.StopWatch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MyQuickSort {
    public void sort(int[] a, int l, int h) {
        if (l < h) { // atleast 2 or more elements
            int j = partition(a, l, h);
            sort(a, l, j);
            sort(a, j+1, h);
        }
    }

    private int partition(int a[], int l, int h) {
        // Pivot
        int pivot = a[l];
        int i = l;
        int j = h;

        while (i < j) {
            for (;i<h;i++) {
                if (a[i] >= pivot) {
                    break;
                }
            }
            for (;j>l;j--) {
                if (a[j] < pivot) {
                    break;
                }
            }
            if (i < j) {
                exch(a, i, j);
            }
        }
        if (j>=l) {
            exch(a, l, j); // replace pivot element
        }
        return j;
    }

    private void exch(int a[], int x, int y) {
        int t = a[x];
        a[x] = a[y];
        a[y] = t;
    }

    public static void main(String args[]) throws InterruptedException {
        //int a[] = {78, 14, 111, 10, 63, 11, 17};
        //int a[] = {1, 4, 2};
        Random random = new Random();
        int noOfInput = 100000;
        int a[] = random.ints(noOfInput, 0, noOfInput ).toArray();
        //Arrays.stream(a).forEach(x->System.out.print(x + " "));
        //System.out.println();

        int len = a.length;
        MyQuickSort qs = new MyQuickSort();
        StopWatch stopwatch = new StopWatch();
        stopwatch.start();
        qs.sort(a, 0, len-1);
        stopwatch.stop();
        long timeTaken = stopwatch.getTime();
        System.out.println("\nTime taken: " + timeTaken + "ms");
        System.out.println("Array len (randomly generated): " + a.length + ", time taken to sort:" + timeTaken + " ms");

        noOfInput = 20000;
        int b[] = new int[noOfInput+1];
        for (int k = 0; k < noOfInput; k++) {
            b[k] = 1;
        }
        Arrays.stream(b).forEach(x->System.out.print(x + " "));
        len = b.length;
        qs = new MyQuickSort();
        stopwatch = new StopWatch();
        stopwatch.start();
        qs.sort(b, 0, len-1);
        stopwatch.stop();
        timeTaken = stopwatch.getTime();
        System.out.println("\nTime taken: " + timeTaken + "ms");
        System.out.println("Array len (already sorted): " + b.length + ", time taken to sort:" + timeTaken + " ms");
        //Arrays.stream(b).forEach(x->System.out.print(x + " "));




    }
}
